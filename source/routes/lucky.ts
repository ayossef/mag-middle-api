import express from 'express';
import controller from '../controllers/lucky';
const router = express.Router();

router.get('/customers', controller.getAllCustomers);

export = router