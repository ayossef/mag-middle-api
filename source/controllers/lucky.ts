/** source/controllers/posts.ts */
import { Request, Response, NextFunction } from 'express';
import axios, { AxiosResponse } from 'axios';

const apiToken = "eyJraWQiOiIxIiwiYWxnIjoiSFMyNTYifQ.eyJ1aWQiOjEsInV0eXBpZCI6MiwiaWF0IjoxNjY1NDM0NzgzLCJleHAiOjE2NjU0MzgzODN9._6VrwNdAzFaD5ie2bR9ZYgNVvaR_8H3qhszJzIHUE0w";
interface Customer {
    id: Number;
    store_id: Number;
    email: String;
    firstname: String;
    lastname: String;
}

const getAllCustomers = async (req: Request, res: Response, next: NextFunction) => {
    // get customers
    const config = {
        method: 'get',
        url: 'http://localhost/rest/all/V1/customers/1',
        headers: { Authorization: `Bearer ${apiToken}` }
    };
    
    // const bodyParameters = {
    //    key: "value"
    // };
    let result: AxiosResponse = await axios.get('http://localhost/rest/all/V1/customers/1',config);
    let customers: [Customer] = result.data;
    return res.status(200).json({
        message: customers
    });
};

export default  {getAllCustomers}